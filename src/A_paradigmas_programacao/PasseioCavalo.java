package A_paradigmas_programacao;

public class PasseioCavalo {
    private int n; // Tamanho do lado do tabuleiro
    private int a[], b[], t[][];

    public PasseioCavalo(int n) {
        this.n = n;
        this.t = new int[n][n];
        this.a = new int[n];
        this.b = new int[n];
        a[0] = 2; a[1] = 1; a[2] = -1; a[3] = -2;
        b[0] = 1; b[1] = 2; b[2] = 2; b[3] = 1;
        a[4] = -2; a[5] = -1; a[6] = 1; a[7] = 2;
        b[4] = -1; b[5] = -2; b[6] = -2; b[7] = -1;
        for (int i = 0; i < n; i++) for (int j = 0; j < n; j++) t[i][j] = 0;
        t[0][0] = 1; // escolhemos uma casa do tabuleiro
    }

    public boolean tenta(int i, int x, int y) {
        int u, v, k;
        boolean q;
        k = -1;  // inicializa selecao de movimentos
        do {
            k = k + 1;
            q = false;
            u = x + a[k];
            v = y + b[k];
      /* Teste para verificar se os limites do tabuleiro 
         serao respeitados. */
            if ((u >= 0) && (u <= 7) && (v >= 0) && (v <= 7))
                if (t[u][v] == 0) {
                    t[u][v] = i;
                    if (i < n * n) { // tabuleiro nao esta cheio
                        q = tenta(i + 1, u, v); // tenta novo movimento
                        if (!q) t[u][v] = 0; // nao sucedido apaga reg. anterior
                    } else q = true;
                }
        } while (!q && (k != 7)); // nao ha casas a visitar a partir de x,y
        return q;
    }

    public void imprimePasseio() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++)
                System.out.print("\t" + this.t[i][j]);
            System.out.println();
        }
    }

    public static void main(String[] args) {
        PasseioCavalo passeioCavalo = new PasseioCavalo(8);
        boolean q = passeioCavalo.tenta(2, 0, 0);
        if (q) passeioCavalo.imprimePasseio();
        else System.out.println("Sem solucao");
    }
}
