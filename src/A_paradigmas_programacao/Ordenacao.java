package A_paradigmas_programacao;

public class Ordenacao {
    private static void merge(int v[], int i, int m, int j) {
        int temp[] = new int[m - i + 1];
        int k;
        for (k = i; k <= m; k++) temp[k - i] = v[k];
        int esq = 0, dir = m + 1;
        k = m - i + 1;
        while (esq < k && dir <= j) {
            if (temp[esq] <= v[dir]) v[i++] = temp[esq++];
            else v[i++] = v[dir++];
        }
        while (esq < k) v[i++] = temp[esq++];
    }

    public static void mergeSort(int v[], int i, int j) {
        if (i < j) {
            int m = (i + j) / 2;
            mergeSort(v, i, m);
            mergeSort(v, m + 1, j);
            merge(v, i, m, j); // Intercala @v@[i..m] e @v@[m+1..j] em @v@[i..j]
        }
    }

    public static void main(String[] args) {
        int v[] = new int[10];
        v[0] = 5;
        v[1] = 12;
        v[2] = 4;
        v[3] = 1;
        v[4] = 9;
        v[5] = 22;
        v[6] = 3;
        v[7] = 11;
        v[8] = 17;
        v[9] = 33;
        Ordenacao.mergeSort(v, 0, 9);
        for (int i = 0; i < 10; i++)
            System.out.println("v[" + i + "]: " + v[i]);
    }
}
