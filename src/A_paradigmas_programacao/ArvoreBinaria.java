package A_paradigmas_programacao;

public class ArvoreBinaria {
    private static class No {
        Object reg;
        No esq, dir;
    }

    private No raiz;

    private void central(No p) {
        if (p != null) {
            central(p.esq);
            System.out.println(p.reg.toString());
            central(p.dir);
        }
    }
}
