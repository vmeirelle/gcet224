package B_estruturas_basicas.testes;

import B_estruturas_basicas.pilhas.arranjos.Pilha;

public class TestaPilhaArranjo {

  public static void main (String[] args) {
    Pilha pilha = new Pilha ();
    Integer vetor[] = new Integer[1000];
    Integer n = null;
    int i, j, k, max;
    java.util.Random random = new java.util.Random ();
    max = 10;
    try {
      // Gera uma permutacao aleatoria de chaves entre 0 e max - 1
      for (i = 0; i < max; i++)
        vetor[i] = new Integer (i);
      for (i = 0; i < max; i++) {
        k = Math.abs (random.nextInt ()) % max;
        j = Math.abs (random.nextInt ()) % max;
        n = vetor[k];
        vetor[k] = vetor[j];
        vetor[j] = n;
      }
      // Insere cada chave na pilha
      for (i = 0; i < max; i++) {
        pilha.empilha (vetor[i]);
        System.out.println ("Empilhou: " + vetor[i].toString ());
      }
      System.out.println ("Tamanho da pilha: " + pilha.tamanho ());
      // Desempilha cada chave
      for (i = 0; i < max; i++) {
        n = (Integer) pilha.desempilha ();
        System.out.println ("Desempilhou: " + n.toString ());
      }
      n = (Integer) pilha.desempilha ();
    } catch (Exception e) {
      System.out.println (e.getMessage ());
    }
    System.out.println ("Tamanho da pilha: " + pilha.tamanho 
        ());
  }

}
