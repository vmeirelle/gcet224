package B_estruturas_basicas.listas.arranjos;

public class Lista {

  private Object item[];
  private int primeiro, ultimo, pos;
  // @{\it Opera\c{c}\~oes}@

  public Lista () { // @{\it Cria uma Lista vazia}@
    this.item = new Object[1000];
    this.pos = -1;
    this.primeiro = 0;
    this.ultimo = this.primeiro;
  }

  public Lista (int maxTam) { // @{\it Cria uma Lista vazia}@
    this.item = new Object[maxTam];
    this.pos = -1;
    this.primeiro = 0;
    this.ultimo = this.primeiro;
  }

  public Object pesquisa (Object chave) {
    if (this.vazia () || chave == null) return null;
    for (int p = 0; p < this.ultimo; p++)
      if (this.item[p].equals (chave)) return this.item[p];
    return null;
  }

  public void insere (Object x) throws Exception {
    if (this.ultimo >= this.item.length)
      throw new Exception ("Erro: A lista esta cheia");
    else {
      this.item[this.ultimo] = x;
      this.ultimo = this.ultimo + 1;
    }
  }

  public Object retira (Object chave) throws Exception {
    if (this.vazia () || chave == null)
      throw new Exception ("Erro : A lista esta vazia");
    int p = 0;
    while (p < this.ultimo && !this.item[p].equals (chave))
      p++;
    if (p >= this.ultimo)
      return null; // @{\it Chave n\~ao encontrada}@
    Object item = this.item[p];
    this.ultimo = this.ultimo - 1;
    for (int aux = p; aux < this.ultimo; aux++)
      this.item[aux] = this.item[aux + 1];
    return item;
  }

  public Object retiraPrimeiro () throws Exception {
    if (this.vazia ())
      throw new Exception ("Erro : A lista esta vazia");
    Object item = this.item[0]; this.ultimo = this.ultimo - 1;
    for (int aux = 0; aux < this.ultimo; aux++)
      this.item[aux] = this.item[aux + 1];
    return item;
  }

  public boolean vazia () {
    return (this.primeiro == this.ultimo);
  }

  public void imprime () {
    for (int aux = this.primeiro; aux < this.ultimo; aux++)
      System.out.println (this.item[aux].toString ());
  }

  public Object primeiro () {
    this.pos = -1; return this.proximo ();
  }

  public Object proximo () {
    this.pos++;
    if (this.pos >= this.ultimo) return null;
    else return this.item[this.pos];
  }

}
