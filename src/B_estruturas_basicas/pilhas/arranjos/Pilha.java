package B_estruturas_basicas.pilhas.arranjos;

public class Pilha {

  private Object item[];
  private int topo;

  // @{\it Opera\c{c}\~oes}@
  public Pilha () { // @{\it Cria uma Pilha vazia}@
    this.item = new Object[1000];
    this.topo = 0;
  }

  public Pilha (int maxTam) { // @{\it Cria uma Pilha vazia}@
    this.item = new Object[maxTam];
    this.topo = 0;
  }

  public void empilha (Object x) throws Exception {
    if (this.topo == this.item.length)
      throw new Exception ("Erro: A pilha esta cheia");
    else
      this.item[this.topo++] = x;
  }

  public Object desempilha () throws Exception {
    if (this.vazia())
      throw new Exception ("Erro: A pilha esta vazia");    
    return this.item[--this.topo];
  }

  public boolean vazia () {
    return (this.topo == 0);
  }

  public int tamanho () {
    return this.topo;
  }

}
